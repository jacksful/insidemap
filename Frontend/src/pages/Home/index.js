import React, { useEffect, useState } from "react";
import { styled } from '@mui/material/styles';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import VisibilityIcon from '@mui/icons-material/Visibility';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import ListItemText from '@mui/material/ListItemText';
import api from "../../services/api";
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Button from '@mui/material/Button';
import CreateProject from "../CreateProject";
const Home = () => {
    const [allProjects, setAllProjects] = useState([]);
    const [createdProjects, setCreatedProjects] = useState([]);
    const [selectedItem, setSelectedItem] = useState([]);
    useEffect(() => {
        getAllProjects();
        getCreatedProjects();

    }, []);

    const getCreatedProjects = async () =>{
        try {
            let data = {
                "status": 'created'
            }
            const getProjects = await api
              .post(`/getProjectByStatus/`, data, {
                headers: {
                  "Content-Type": `application/json; boundary=${data._boundary}`,
                },
              })
              .catch((err) => {
                console.log("Err: ", err);
              });
              console.log(getProjects);

          let allData = getProjects.data.result;
          setCreatedProjects(allData);
          
        } catch (error) {
          console.error("Error,single: ", error);
        }
    }


    const getAllProjects = async () =>{
        try {
            const getProjects = await api
              .get(`/getProjects/`)
              .catch((err) => {
                console.log("Err: ", err);
              });
              console.log(getProjects);

          let allData = getProjects.data.result;
          setAllProjects(allData);
          
        } catch (error) {
          console.error("Error,single: ", error);
        }
    }

    const [open, setOpen] = useState(false);

    const handleClickOpen = (data) => {
        setOpen(true);
        setSelectedItem(data);
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <>
        <Container maxWidth="xl">
            <Grid container spacing={2}>
                <Grid item xs={3}>
                    <div style={{background: "#fff"}}>
                        <h2 className='step-title all'>All</h2>
                        <List>
                            {Object.keys(allProjects).map((key) => (
                                <ListItem key={key}>
                                    <ListItemAvatar>
                                        <VisibilityIcon style={{cursor: 'pointer'}}  onClick={() => handleClickOpen(allProjects[key])} />
                                    </ListItemAvatar>
                                    <ListItemText
                                        primary={allProjects[key].name}
                                    />
                                </ListItem>
                                
                            ))}
                        </List>
                    </div>
                </Grid>
                <CreateProject getAllProjects={getAllProjects} />
            </Grid>
        </Container>
        
        <Dialog
          open={open}
          onClose={handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
        <DialogTitle id="alert-dialog-title">
          {selectedItem.name}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {selectedItem.description}
            <br />
            <small>Status: {selectedItem.status}</small>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Close</Button>
        
        </DialogActions>
      </Dialog>

        </>
    )
}
export default Home;