import { Button, FormControl, Grid, InputLabel, MenuItem, Select, TextareaAutosize } from '@mui/material';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Swal from 'sweetalert2';
import api from "../../services/api";
import { useState } from 'react';
const CreateProject = ({getAllProjects}) => {
    const [status, setStatus] = useState('');
    const [formData, setFormData] = useState({
        name: "",
        description: "",
        status: "created",
        type: 'test'
    });

    const handleChangeStatus = (selectedOption) => {
        let data = formData;
        data.status = selectedOption.target.value;
        setFormData(data);
    };
    async function handleInputChange(event) {
        let { name, value } = event.target;
        setFormData({ ...formData, [name]: value });
    }
    const submitProject = async (event)=> {
        event.preventDefault();
        console.log(formData)

        
        try{
           
            const response = await api.post("/createProject", formData, {
                headers: {
                  "Content-Type": `application/json; boundary=${formData._boundary}`,
                },
              });

              if(response.data.response){
                Swal.fire({
                    title: '<strong>Project successfully Created!</strong>',
                    icon: 'success',
                    showCloseButton: false,
                    showCancelButton: false
                  }).then((result) => {
                    getAllProjects();
                    setFormData({name: "",
                    description: "",
                    status: "",
                    type: 'test'})
                });
              }else{
                Swal.fire({
                    title: '<strong>Error</strong>',
                    icon: 'warning',
                    showCloseButton: false,
                    showCancelButton: false
                  })
              }
            
            console.log(response);
        }catch(error){
            Swal.fire({
                title: '<strong>Error</strong>',
                icon: 'danger',
                showCloseButton: false,
                showCancelButton: false
              })
        }
    }
    return (
        <Grid item xs={6}>
            <form onSubmit={submitProject}>
            <h3>Create Project</h3>
            <Box
            sx={{ minWidth: 120 }}
            >
                <FormControl fullWidth>
                    <TextField required onChange={handleInputChange} value={formData.name} name='name' id="standard-basic" label="Name" variant="outlined" />
                </FormControl>
            </Box>
            <br />
            <Box sx={{ minWidth: 120 }}>
                <FormControl fullWidth>
                    <InputLabel id="demo-simple-select-label">Status</InputLabel>
                    <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    label="Age"
                    onChange={handleChangeStatus}
                    name="status"
                    required
                    >
                    <MenuItem value={'created'}>Created</MenuItem>
                    <MenuItem value={'processing'}>Processing</MenuItem>
                    <MenuItem value={'done'}>Finished</MenuItem>
                    </Select>
                </FormControl>
            </Box>
            <br />
            <Box>
                <FormControl fullWidth>
                <TextareaAutosize
                    name="description"
                    aria-label="minimum height"
                    minRows={5}
                    onChange={handleInputChange}
                    placeholder="Description"
                    style={{ width: "100%" }}
                    required
                    />
                </FormControl>
            </Box>
            <br />
            <Button type="submit" variant="contained">Save</Button>
            </form>
        </Grid>
    )
}

export default CreateProject;