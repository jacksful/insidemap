import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from "@mui/material";
import React, { useEffect, useState } from "react";
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
import VisibilityIcon from '@mui/icons-material/Visibility';
import api from "../../services/api";
const uuid = () =>{
    return Math.floor(new Date().valueOf() * Math.random());
}






const ProjectManage = () => {

    const [allProjects, setAllProjects] = useState([]);
    let columnsFromBackend = {
        [uuid()]: {
          name: "Created",
          items: [],
          status: 'created'
        },
        [uuid()]: {
          name: "Processing",
          items: [],
          status: 'processing'
        },
        [uuid()]: {
          name: "Finished",
          items: [],
          status: 'done'
        }
    };
    const [selectedItem, setSelectedItem] = useState([]);

    useEffect(() => {
        getAllProjects();
    }, [setAllProjects]);

    const getAllProjects = async () =>{
        try {
            const getProjects = await api
              .get(`/getProjects/`)
              .catch((err) => {
                console.log("Err: ", err);
              });
              console.log(getProjects);
              
                let allData = getProjects.data.result;
                console.log(allData);
                console.log("allData");
                setAllProjects(allData);

                let createdProject = allData.filter(function (el) {
                    return el.status === "created"
                });
                let processingProjects = allData.filter(function (el) {
                    return el.status === "processing"
                });
                let finishedProject = allData.filter(function (el) {
                    return el.status === "done"
                });

                let colomn = {
                    [uuid()]: {
                        name: "Created",
                        items: createdProject,
                        status: 'created'
                    },
                    [uuid()]: {
                        name: "Processing",
                        items: processingProjects,
                        status: 'processing'
                    },
                    [uuid()]: {
                        name: "Finished",
                        items: finishedProject,
                        status: 'done'
                    }
                }
                setColumns(colomn);
          
        } catch (error) {
          console.error("Error,single: ", error);
        }
    }



    const [columns, setColumns] = useState(columnsFromBackend);
    
    const onDragEnd = (result, columns, setColumns) => {
        debugger;
      if (!result.destination) return;
      const { source, destination } = result;
    
      if (source.droppableId !== destination.droppableId) {
        const sourceColumn = columns[source.droppableId];
        const destColumn = columns[destination.droppableId];
        const status = columns[destination.droppableId].status;
        const draggableId = result.draggableId;
        updateStatus(status, draggableId);
        const sourceItems = [...sourceColumn.items];
        const destItems = [...destColumn.items];
        const [removed] = sourceItems.splice(source.index, 1);
        destItems.splice(destination.index, 0, removed);
        setColumns({
          ...columns,
          [source.droppableId]: {
            ...sourceColumn,
            items: sourceItems
          },
          [destination.droppableId]: {
            ...destColumn,
            items: destItems
          }
        });
      } else {
        const column = columns[source.droppableId];
        const copiedItems = [...column.items];
        const [removed] = copiedItems.splice(source.index, 1);
        copiedItems.splice(destination.index, 0, removed);
        setColumns({
          ...columns,
          [source.droppableId]: {
            ...column,
            items: copiedItems
          }
        });
      }
    };

    const updateStatus = async (status,id) =>{
        let data = {
            "status": status,
            "id": id
        };
        const response = await api.post("/updateProjectStatus", data, {
            headers: {
              "Content-Type": `application/json; boundary=${data._boundary}`,
            },
          });
        
        console.log(response);
    }

    const [open, setOpen] = useState(false);

    const handleClickOpen = (data) => {
        setOpen(true);
        setSelectedItem(data);
    };

    const handleClose = () => {
        setOpen(false);
    };
  
  return (
    <div style={{ display: "flex", justifyContent: "center", height: "100%" }}>
      <DragDropContext
        onDragEnd={result => onDragEnd(result, columns, setColumns)}
      >
        {Object.entries(columns).map(([columnId, column], index) => {
          return (
            <div
              style={{
                padding: "10px"
              }}
              key={columnId}
            >
              <h2 className={'step-title '+column.status}>{column.name}</h2>
              <div>
                <Droppable droppableId={columnId} key={columnId}>
                  {(provided, snapshot) => {
                    return (
                      <div
                        {...provided.droppableProps}
                        ref={provided.innerRef}
                        style={{
                          background: snapshot.isDraggingOver
                            ? "lightblue"
                            : "lightgrey",
                          padding: 4,
                          width: 250,
                          minHeight: 500
                        }}

                      >
                        {column.items.map((item, index) => {
                          return (
                            <Draggable
                              key={item._id}
                              draggableId={item._id}
                              index={index}
                            >
                              {(provided, snapshot) => {
                                return (
                                  <div
                                    ref={provided.innerRef}
                                    {...provided.draggableProps}
                                    {...provided.dragHandleProps}
                                    style={{
                                      userSelect: "none",
                                      padding: '10px',
                                      minHeight: "50px",
                                      marginBottom: "5px",
                                      color: "white",
                                      ...provided.draggableProps.style
                                    }}
                                    className={column.status}
                                  >
                                    <div style={{textAlign:'left'}}>
                                        <strong>{item.name}</strong>
                                    </div>
                                    <br />
                                    <div style={{textAlign:'right', display:'flex'}}>
                                        <VisibilityIcon style={{cursor: 'pointer'}} onClick={() => handleClickOpen(item)} />
                                        <code style={{background: "#fff", 
                                            color: "#000", padding: "2px", margin: "5px", fontSize: '10px', borderRadius: '5px'}}>
                                            {item.status}
                                        </code>
                                    </div>
                                    
                                  </div>
                                );
                              }}
                            </Draggable>
                          );
                        })}
                        {provided.placeholder}
                      </div>
                    );
                  }}
                </Droppable>
              </div>
            </div>
          );
        })}
      </DragDropContext>


      <Dialog
          open={open}
          onClose={handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
        <DialogTitle id="alert-dialog-title">
          {selectedItem.name}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {selectedItem.description}
            <br />
            <small>Status: {selectedItem.status}</small>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Close</Button>
        
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default ProjectManage;
