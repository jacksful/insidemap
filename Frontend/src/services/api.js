import axios from 'axios';

const api = axios.create({
  cache: false,
  baseURL: "http://localhost:4444/"

});

export default api;