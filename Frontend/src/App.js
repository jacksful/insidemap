import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import logo from './logo.svg';
import './App.css';
import Home from "./pages/Home/index";
import Header from "./components/Header/index";
import ProjectManage from './pages/ProjectManage';
import CreateProject from './pages/CreateProject';
function App() {
  return (
    <div className="App">
      <Router>
          <Header />
        
          <main style={{marginTop: "20px"}}>
          <Routes>
            <Route exact path="/" element={<Home/>}/>
            <Route exact path="/Home" element={<Home/>}/>
            <Route exact path="/Projects" element={<ProjectManage/>}/>
            <Route exact path="/Create" element={<CreateProject/>}/>
            {/* <Route path="*" element={<NotFound/>}/> */}
          </Routes>
          </main>
      </Router>
    </div>
  );
}

export default App;
