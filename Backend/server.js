const express = require('express');
var cors = require('cors');
const mongoose = require('mongoose');

const app = express();

const dburl= "mongodb+srv://jacksful:Jacksful131@cluster0.mc2um.mongodb.net/?retryWrites=true&w=majority"

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
//app.use('/images', express.static('imgs'));
// app.use('/build', express.static('./build'));
// app.use('/download', express.static('./download'));
// app.use('/Uploads', express.static('./Uploads'));
const routes = require('./routes')(app);
mongoose.connect(dburl, { useNewUrlParser: true, useUniFiedTopology: true })
  .then((result) =>  app.listen(4444, () => {
  console.log('Listening on port 4444');
}))
  .catch((err) => console.log(err));
// const server = app.listen(3333, () => {
//   console.log('Listening on port %s...', server.address().port);
// });
