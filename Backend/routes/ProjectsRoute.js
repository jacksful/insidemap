const Projects = require('../models/projects')
const nodemailer = require('nodemailer');
const schedule = require('node-schedule');
const axios = require('axios');
var validator = require("email-validator");
const ProjectsRoute = (app) => {

    app.post('/createProject', async (req, res) => {
        try {
            const { name, description, type, status } = req.body;
            //let encryptedPassword = await bcrypt.hash(password, 10);
            const project = new Projects({
                name: name,
                description: description,
                type: type,
                status: status
            })
            project.save()
                .then((result) => {
                    res.status(201).json({ response: true, result });
                })
                .catch((err) => {
                    res.status(201).json({ 
                        response: false, 
                        data: [],
                        message: err
                    });
                })
        } catch (err) {
            res.status(201).json({ 
                response: false, 
                data: [],
                message: err
            });
            console.log(err);
        }
        
    
    })


    app.get('/getProjects', async (req, res) => {
        try {
            Projects.find()
                .then((result) => {
                    res.status(201).json({ response: true, result });
                })
                .catch((err) => {
                    res.status(201).json({ 
                        response: false, 
                        data: [],
                        message: err
                    });
                })
        } catch (err) {
            res.status(201).json({ 
                response: false, 
                data: [],
                message: err
            });
            console.log(err);
        }
        
    
    });

    app.post('/getProjectByStatus', async (req, res) => {
        try {
            const { status } = req.body;
            Projects.find().where('status').equals(status)
                .then((result) => {
                    res.status(201).json({ response: true, result });
                })
                .catch((err) => {
                    res.status(201).json({ 
                        response: false, 
                        data: [],
                        message: err
                    });
                })
        } catch (err) {
            res.status(201).json({ 
                response: false, 
                data: [],
                message: err
            });
            console.log(err);
        }
        
    
    });

    app.post('/updateProjectStatus', async (req, res) => {
        try {
            const { status, id } = req.body;
            Projects.updateOne(
                { "_id" : id },
                { $set: { "status" : status } }
             ).then((result) => {
                    mailAlart(status);
                    res.status(201).json({ response: true, result });
                })
                .catch((err) => {
                    res.status(201).json({ 
                        response: false, 
                        data: [],
                        message: err
                    });
                })
        } catch (err) {
            res.status(201).json({ 
                response: false, 
                data: [],
                message: err
            });
            console.log(err);
        }
        
    
    });

    const mailAlart = async(status) =>{
        let mymail = 'fahimzzz.mk@gmail.com';
        let transporter = nodemailer.createTransport({
            host: 'smtp.gmail.com',
            port: 587,
            secure: false,
            requireTLS: true,
            auth: {
                user: mymail,
                pass: 'FahimTT#1'
            }
            });

            let mailOptions = {
                from: mymail,
                to: 'pekecij807@iconzap.com',
                subject: 'Alart Project Status',
                //text: JSON.stringify(response.data)
                html: '<p><strong>Hello Sir</strong> </p>'+' Your project has been ' + status
            };
  
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error.message);
            }
            console.log('success');
        });
    }

   


    

    
    // app.post('/login', async (req, res) => {
    //     try {
    //         const { email, password } = req.body;
    //         const user = await User.findOne({ Email:email });

    //         if (user!=null) {
    //             if (user.Password != password) {
    //                 res.status(201).json({message:"Invalid Email or Password",response:false})
                
    //             }
    //             else {
    //                 let data = {
    //                     username: user.Name, 
    //                     email: user.Email, 
    //                     Gasthreshold: user.Gasthreshold
    //                 }

    //                 res.status(201).json({message:"Valid User",response:true, data})
                
    //             }
            
    //         } else {
    //             res.status(201).json({message:"Invalid Email or Password",response:false})
            
    //         }
            
            
            
        
    //     } catch (err) {
    //         console.log(err);
    //     }
    // })
    
    // app.post('/updateGasthreshold', async (req, res) => {
        
    //     try {
    //         const { email, gasthreshold } = req.body;
    //         const filter = { Email: email };
    //         const update = { Gasthreshold: Number(gasthreshold) };
    //         const user = await User.findOneAndUpdate(filter, update);
    //         if(user!=null){
    //             res.status(201).json({message:"Updated",response:true});
    //         }
    //         else {
    //             res.status(201).json({message:"User Not Found",response:false})
            
    //         }
            
            
            
            
            
    //     } catch (err) {
    //         console.log(err);
    //     }
        
    // })
    
  

}

module.exports = ProjectsRoute;