const User = require('../models/user')
const nodemailer = require('nodemailer');
const schedule = require('node-schedule');
const axios = require('axios');
var validator = require("email-validator");
const userRoute = (app) => {
    

    app.post('/register', async (req, res) => {
        try {
            const { name, email, password } = req.body;
            //let encryptedPassword = await bcrypt.hash(password, 10);
            let exists = false;
            
            let validate= validator.validate(email); 
            if (validate) {
                await User.find({ Email: email })
                    .then((result) => {
                        if (result.length != 0) {
                            exists = true;
                        }
                
                    }).catch((err) => {
                        console.log(err);
                    })
                if (exists) {
                    res.status(201).json({ message: "Already Exists", response: false });
            
                }
                else {
                    const user = new User({
                        Name: name,
                        Email: email.toLowerCase(),
                        Password: password,
                        Gasthreshold: 0
                    })
                    user.save()
                        .then((result) => {
                            res.status(201).json({ response: true, result });
                        })
                        .catch((err) => {
                            console.log(err);
                        })
                }
            }
            else {
                 res.status(201).json({ response: false, message: "Invalid Email address" });
            }
        } catch (err) {
            console.log(err);
        }
        
    
    })
    
    app.post('/login', async (req, res) => {
        try {
            const { email, password } = req.body;
            const user = await User.findOne({ Email:email });

            if (user!=null) {
                if (user.Password != password) {
                    res.status(201).json({message:"Invalid Email or Password",response:false})
                
                }
                else {
                    let data = {
                        username: user.Name, 
                        email: user.Email, 
                        Gasthreshold: user.Gasthreshold
                    }

                    res.status(201).json({message:"Valid User",response:true, data})
                
                }
            
            } else {
                res.status(201).json({message:"Invalid Email or Password",response:false})
            
            }
            
            
            
        
        } catch (err) {
            console.log(err);
        }
    })
    
  

}

module.exports = userRoute;