const user = require('./userRoute');
const ProjectsRoute = require('./ProjectsRoute');

const appRouter = (app) => {
  user(app);
  ProjectsRoute(app);
};

module.exports = appRouter;